import Vue from 'vue';
import App from './App.vue';

import router from './router';

import '@babel/polyfill';
import './plugins/vuetify';

import LogManager from './core/LogManager';

Vue.config.productionTip = false
LogManager.enabled = true;

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
