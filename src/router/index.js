import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/components/Home';
import Login from '@/components/auth/Login';
import Register from '@/components/auth/Register';
import Profile from '@/components/profile/Profile';

import store from '../plugins/store';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
        {
            path: '/',
            component: Home,
            meta: { authRequred: true },
        },
        {
            path: '/profile',
            component: Profile,
            meta: { authRequred: true },
        },
        {
            path: '/login',
            component: Login,
        },
        {
            path: '/register',
            component: Register,
        },
        {
            path: '**',
            redirect: '/',
        },
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.authRequred)) {
        if (!store.state.user) {
            next({ path: '/login' })
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;
