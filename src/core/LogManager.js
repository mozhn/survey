import Console from 'console';

export default class LogManager {
    static enabled = false;

    log(result) {
        if (LogManager.enabled) {
            Console.log(result);
        }
    }

    warn(result) {
        if (LogManager.enabled) {
            Console.warn(result);
        }
    }

    info(result) {
        if (LogManager.enabled) {
            Console.info(result);
        }
    }

    error(result) {
        if (LogManager.enabled) {
            Console.error(result);
        }
    }
}
