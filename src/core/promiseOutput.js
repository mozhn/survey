import LogManager from './LogManager';

export default function(method, operation) {
    const logger = new LogManager();

    return method.then((res) => {
        logger.log({ response: res, process: operation, success: true });
        return { response: res, process: operation, success: true };
    }).catch((err) => {
        logger.log({ response: err, process: operation, success: false });
        return { response: err, process: operation, success: false };
    })
}
