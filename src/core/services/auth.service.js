import { auth, firestore } from 'firebase';

import promiseOutput from '../promiseOutput';

const OPERATIONS = {
    AUTHENTICATION_REGISTER: 'user register (authentication)',
    FIRESTORE_REGISTER: 'user register (firestore)',
    LOGOUT: 'user logout',
    LOGIN: 'user login'
};

const AuthService = {
    register(email, password) {
        return promiseOutput(
            auth().createUserWithEmailAndPassword(email, password),
            OPERATIONS.AUTHENTICATION_REGISTER,
        ).then((authResult) => {
            if (!authResult.response.user) {
                return authResult;
            }

            const res = authResult.response.user;
            const user = {
                nickname: '', image: '',
                email: res.email, creationTime: res.metadata.creationTime,
            };

            return promiseOutput(
                firestore().collection('users').doc(res.uid).set(user),
                OPERATIONS.FIRESTORE_REGISTER,
            );
        });
    },
    login(email, password) {
        return promiseOutput(
            auth().signInWithEmailAndPassword(email, password),
            OPERATIONS.LOGIN,
        );
    },
    logout() {
        return promiseOutput(
            auth().signOut(), OPERATIONS.LOGOUT,
        );
    }
}

export default AuthService;
