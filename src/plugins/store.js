import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
    user: null,
};

const mutations = {
    authenticationUpdate(state, { user }) {
        Vue.set(state, 'user', user);
    }
}

const actions = {};

const getters = {
    user: state => state.user,
};

export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters,
});
