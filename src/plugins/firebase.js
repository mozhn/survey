import Vue from 'vue';
import VueFire from 'vuefire';
import Firebase from 'firebase';
import store from './store';

Vue.use(VueFire);

Firebase.initializeApp({
    apiKey: 'AIzaSyA50gXaObFjLGKC9HyTrE5ZuOry9CJkSxs',
    authDomain: 'vue-survey.firebaseapp.com',
    databaseURL: 'https://vue-survey.firebaseio.com',
    projectId: 'vue-survey',
    storageBucket: 'vue-survey.appspot.com',
    messagingSenderId: '587437306978',
});

export const fireStore = Firebase.firestore();
export const timeStamp = Firebase.firestore.Timestamp;

fireStore.settings({ timestampsInSnapshots: true, });

Firebase.auth().onAuthStateChanged((user) => store.commit('authenticationUpdate', { user }));
